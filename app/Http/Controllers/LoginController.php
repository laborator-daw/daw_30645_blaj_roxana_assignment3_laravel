<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\UserData;

class LoginController extends Controller
{
    function userLogout(){
        if(session()->has('user')){
            session()->pull('user');
        }
        return "user logout";
    }


    function userLogin(Request $request){

        //check-user
        $validator = Validator::make($request->all(),
        [
            "email" => "required|email",
            "password" => "required"
        ]);

        if($validator->fails()) {
            return response()->json(["status" => "failed", "message" => "Login credentials are not valid"]);
        }

        $email_status = UserData::where("email", $request->email)->first();

        if(!is_null($email_status)) {
            $password_status = UserData::where("email", $request->email)->where("password", md5($request->password))->first();

            if(!is_null($password_status)) {
                $user = UserData::where("email", $request->email)->first();
                $data = $request->input(); 
                session()->put('user', $data['email']);
                return response()->json([ "status"=> "ok", "message" => "You have logged in successfully", "data" => $user], 200);
            }
            else {
                return response()->json(["status" => "failed", "message" => "Unable to login. Incorrect password."]);
            }
        } else {
            return response()->json(["status" => "failed",  "message" => "Unable to login. Email doesn't exist."]);
        }
        
        
        //return $request->session()->get('data');
    }
}
