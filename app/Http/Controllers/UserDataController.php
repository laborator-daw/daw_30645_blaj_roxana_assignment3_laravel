<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use App\Models\UserData;


class UserDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = UserData::all();
        return response()->json(["data" => $user]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            "name" =>"required",
            "email"=>"required|email|unique:user_data",
            "password"=>"required|min:5",
            "role"=>"required"
        ]);

        if($validator->fails()) {
            return response()->json(["status" => "failed", "message" => "User data is not valid"]);
        }

       

        $user = new UserData([
            'name' => $request->name,
            'email' => $request->email,
            'password' => md5($request->password),
            'role' => $request->role
        ]);
        if($user->save()){
            return response()->json(["status" => "created",
                "message" => "User record created"
            ], 201);
        }else{
            return response()->json(["status"=>"failed", "message" => "Whoops! User already exists"], 404);
        }
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user        =       UserData::find($id);
        if(!is_null($user)) {
            return response()->json($user);
        }
        else {
            return response()->json(["message" => "Whoops! User not found"], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        if (UserData::where('id', $id)->exists()) {
            $user = UserData::find($id);

            if(!is_null($request->servMed)){
               
                $services = explode(', ', $user->servMed);
                $same = 0;
                
                foreach($services as $i =>$key) {
                    $i >0;
                    if($key == $request->servMed){
                        $user->servMed = $user->servMed;
                        $same = 1;
                    }
                }
               
                if($same == 0){
                    if(!is_null($user->servMed))
                        $allServMed = $user->servMed . ', ' . $request->servMed;
                    else
                        $allServMed = $request->servMed;
                    $user->servMed = $allServMed;
                }
            }
    
            $user->name = is_null($request->name) ? $user->name : $request->name;
            $user->email = is_null($request->email) ? $user->email : $request->email;
           
            //echo($request->file("gdprFile"));
            
           
            $user->save();
    
            return response()->json([
                $user
            ], 200);
          } else {
            return response()->json(["status"=>"failed",
              "message" => "User not found"
            ], 404);
          }

          
    }

    public function updatePassword(Request $request, $id)
    {
        if (UserData::where('id', $id)->exists()) {
            $user = UserData::find($id);
            $user->password = is_null($request->password) ? $user->password :  md5($request->password);
            $user->save();
            return response()->json([
                $user
            ], 200);
        }
        else {
            return response()->json(["status"=>"failed",
              "message" => "User not found"
            ], 404);
        }
    }

    public function updateFile(Request $request, $id)
    {
        //echo($request);
        if (UserData::where('id', $id)->exists()) {
            $user = UserData::find($id);
        
            if($request->hasFile('gdprFile')){ 
               
                $file = $request->file('gdprFile')->getClientOriginalName();
                $filename = pathinfo($file, PATHINFO_FILENAME);
                $extension = $request->file('gdprFile')->getClientOriginalExtension();
                $filenameToStore = 'gdpr_'.$filename.'_'.time().'.'.$extension;
            
                $path = $request->file('gdprFile')->move('public/gdprFiles', $filenameToStore);
       
                $user->gdprFile = is_null($filename) ? $user->gdprFile : $filenameToStore; 
                $user->save();

                return response()->json([
                    $user
                ], 200);
            }
        }
       
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(UserData::where('id', $id)->exists()) {
            $user = UserData::find($id);
            $user->delete();
    
            return response()->json([
              "message" => "User deleted succesfully"
            ], 202);
          } else {
            return response()->json([
              "message" => "User not found"
            ], 404);
          }
    }
}
