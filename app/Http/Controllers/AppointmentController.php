<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Appointment;
use Illuminate\Support\Facades\DB;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointment = Appointment::all();
        return response()->json(["data" => $appointment]);
    }

    public function getAppointmentForDoctor($id){
        $appDr =  DB::table('appointments')->where('doctor', $id)->get();
        return response()->json(["data" => $appDr]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            "appointmentDate" =>"required|date",
            "appointmentHour" =>"required",
            "patient"=>"required",
            "doctor"=>"required",
            "medServApp"=>"required",

          
        ]);

        if($validator->fails()) {
            echo('failed');
            return response()->json(["status" => "failed", "message" => "Appointment data is not valid"]);
        }

        $count = DB::table('appointments')->where('appointmentDate', $request->appointmentDate)
        ->where('appointmentHour', $request->appointmentHour)
        ->where('doctor', $request->doctor)
        ->count();

        if($count != 0 ){
            return response()->json(["status" => "failed", "message" => "Appointment data is not valid, the selected date is taken"], 200);
        }

//before store verify if allready exists

      
        $appointment = new Appointment([
            'appointmentDate' => $request->appointmentDate,
            'appointmentHour' => $request->appointmentHour,
            'patient' => $request->patient,
            'doctor' => $request->doctor,
            'medServApp' => $request->medServApp
        ]);

        if($appointment->save()){
            return response()->json([
                "message" => "New appointment created"
            ], 201);
        }else{
            echo("ups");
            return response()->json(["message" => "Whoops! Something went wrong, please try again"], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Appointment::where('id', $id)->exists()) {
            $appointment = Appointment::find($id);
            $appointment->delete();
    
            return response()->json([
              "message" => "Appointment deleted succesfully"
            ], 202);
          } else {
            return response()->json([
              "message" => "Appointment not found"
            ], 404);
          }
    }
}
