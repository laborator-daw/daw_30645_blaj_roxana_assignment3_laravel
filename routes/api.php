<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserDataController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});

Route::post('/login',  [LoginController::class, 'userLogin']);
Route::get('/logout',  [LoginController::class, 'userLogout']);
Route::post('/updateFile/{userdatum}',  [UserDataController::class, 'updateFile']);
Route::put('/updatePassword/{userdatum}',  [UserDataController::class, 'updatePassword']);
Route::get('/getAppointmentForDoctor/{userdatum}',  [AppointmentController::class, 'getAppointmentForDoctor']);
Route::apiResource('userdata', 'App\Http\Controllers\UserDataController');
Route::apiResource('appointment', 'App\Http\Controllers\AppointmentController');
